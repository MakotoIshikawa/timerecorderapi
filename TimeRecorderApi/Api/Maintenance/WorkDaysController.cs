﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using AmsDatabaseConnectionLibrary;
using AmsDatabaseConnectionLibrary.Model.Common;
using ExtensionsLibrary.Extensions;
using TimeRecorderApi.Api.Primitive;
using static System.Web.VirtualPathUtility;

namespace TimeRecorderApi.Api.Maintenance {
	/// <summary>
	/// 日次勤怠情報のWeb API を提供します。
	/// </summary>
	[RoutePrefix("Api/WorkDays")]
	public class WorkDaysController : ApiControllerBase {
		#region プロパティ

		/// <summary>
		/// ユーザーID
		/// </summary>
		/// <remarks>
		/// API から変更された時用のユーザーIDです。
		/// </remarks>
		protected string UserNum => "ApiWorkDay";

		#endregion

		#region メソッド

		#region GET

		/// <summary>
		/// [GET]
		/// </summary>
		/// <returns>日次勤怠情報のコレクション</returns>
		[HttpGet]
		[Route("")]
		public IEnumerable<t_work_d> GetValues() {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					return db.t_work_d.ToList();
				}
			} catch (Exception) {
				return Enumerable.Empty<t_work_d>();
			}
		}

		/// <summary>
		/// [GET]
		/// </summary>
		/// <param name="id">ユーザーID</param>
		/// <param name="day">勤務日</param>
		/// <returns>日次勤怠情報</returns>
		[HttpGet]
		[Route("{id}/{day}")]
		[ResponseType(typeof(t_work_d))]
		public IHttpActionResult GetValue(string id, DateTime day) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var item = db.t_work_d.SingleOrDefault(d => d.syain_no == id & d.work_day == day);
					if (item == null) {
						return this.NotFound();
					}

					return this.Ok(item);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		#endregion

		#region POST

		/// <summary>
		/// [POST]
		/// </summary>
		/// <param name="value">日次勤怠情報</param>
		/// <returns>応答メッセージを返します。</returns>
		[HttpPost]
		[Route("")]
		[ResponseType(typeof(t_work_d))]
		public IHttpActionResult PostValue([FromBody()] t_work_d value) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var id = value.syain_no;
					var nendo = value.nendo;
					var getsudo = value.getsudo;
					var work_day = value.work_day;
					if (id == null || nendo == null || getsudo == null || work_day == null) {
						return this.Forbidden("必須項目が設定されていません。");
					}

					var root = AppendTrailingSlash(this.Request.RequestUri.AbsoluteUri);
					var location = $"{root}{id}/{work_day.ToString("yyyy-MM-dd")}";
					if (db.t_work_d.Any(u => u.syain_no == id && u.work_day == work_day)) {
						return this.Redirect(location);
					}

					// 作成日時
					var created = DateTime.Now;

					value.ins_date = created;
					value.ins_syain_no = this.UserNum;
					value.upd_date = created;
					value.upd_syain_no = this.UserNum;

					db.t_work_d.InsertOnSubmit(value);

					db.SubmitChanges();

					return this.Created(location, value);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		#endregion

		#region PUT

		/// <summary>
		/// [PUT]
		/// ユーザーIDと勤務日を指定して、
		/// 日次勤怠情報を更新します。
		/// </summary>
		/// <param name="id">ユーザーID</param>
		/// <param name="day">勤務日</param>
		/// <param name="value">日次勤怠情報</param>
		/// <returns>応答メッセージを返します。</returns>
		[HttpPut]
		[Route("{id}/{day}")]
		[ResponseType(typeof(t_work_d))]
		public IHttpActionResult PutValue(string id, DateTime day, [FromBody()] t_work_d value) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var item = db.t_work_d.SingleOrDefault(d => d.syain_no == id & d.work_day == day);
					if (item == null) {
						return this.NotFound();
					}

					var modified = false;

					item.PropertyChanged += (sender, e) => {
						modified = true;
					};

					if (value.del_syain_no.IsWhiteSpace()) {
						item.del_date = null;
						item.del_syain_no = null;
					}

					if (value.nendo != null) { item.nendo = value.nendo; }
					if (value.getsudo != null) { item.getsudo = value.getsudo; }
					if (value.kintai_kbn_cd != null) { item.kintai_kbn_cd = value.kintai_kbn_cd; }
					if (value.standard_time != null) { item.standard_time = value.standard_time; }
					if (value.kouban_in_time != null) { item.kouban_in_time = value.kouban_in_time; }
					if (value.kouban_out_time != null) { item.kouban_out_time = value.kouban_out_time; }
					if (value.kouban_rest_time != null) { item.kouban_rest_time = value.kouban_rest_time; }
					if (value.access_in_time != null) { item.access_in_time = value.access_in_time; }
					if (value.access_out_time != null) { item.access_out_time = value.access_out_time; }
					if (value.in_kintai_kbn_cd != null) { item.in_kintai_kbn_cd = value.in_kintai_kbn_cd; }
					if (value.jisseki_in_time != null) { item.jisseki_in_time = value.jisseki_in_time; }
					if (value.out_kintai_kbn_cd != null) { item.out_kintai_kbn_cd = value.out_kintai_kbn_cd; }
					if (value.jisseki_out_time != null) { item.jisseki_out_time = value.jisseki_out_time; }
					if (value.jisseki_rest_time != null) { item.jisseki_rest_time = value.jisseki_rest_time; }
					if (value.work_time != null) { item.work_time = value.work_time; }
					if (value.reason != null) { item.reason = value.reason; }
					if (value.jisseki_approval_flg != null) { item.jisseki_approval_flg = value.jisseki_approval_flg; }
					if (value.kouban_approval_flg != null) { item.kouban_approval_flg = value.kouban_approval_flg; }

					if (!modified) {
						return this.StatusCode(HttpStatusCode.NotModified);
					}

					item.upd_date = DateTime.Now;
					item.upd_syain_no = this.UserNum;

					db.SubmitChanges();

					return this.Ok(item);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		/// <summary>
		/// [PUT]
		/// ユーザーIDと勤務日を指定して、
		/// 日次勤怠情報の出退勤時刻を更新します。
		/// </summary>
		/// <param name="id">ユーザーID</param>
		/// <param name="day">勤務日</param>
		/// <param name="value">入退室情報</param>
		/// <returns>応答メッセージを返します。</returns>
		[HttpPut]
		[Route("Record/{id}/{day}")]
		[ResponseType(typeof(t_work_d))]
		public IHttpActionResult PutRecord(string id, DateTime day, [FromBody()] AttendanceInfo value) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var item = db.t_work_d.SingleOrDefault(d => d.syain_no == id & d.work_day == day);
					if (item == null) {
						return this.NotFound();
					}

					var modified = false;

					item.PropertyChanged += (sender, e) => {
						modified = true;
					};

					if (item.access_in_time.IsEmpty()) {
						item.access_in_time = value.access_in_time;
#if true
						// スワン用
						var inTime = item.kouban_in_time.ToTimeSpan();
						if (inTime.HasValue) {
							// 交番に設定されている出勤時刻で揃える。
							item.access_in_time = inTime.Value.ToHourAndMinString();
						}
#endif
					}
					item.access_out_time = value.access_out_time;

					var workTime = item.OutTime - item.InTime;
					if (workTime < TimeSpan.Zero) {
						throw new InvalidOperationException("入室時刻が退室時刻よりも後に設定されています。");
					}

					var restTime = item.kouban_rest_time.ToTimeSpan();
					workTime -= restTime;

					item.work_time = workTime?.ToHourAndMinString();

					if (!modified) {
						return this.StatusCode(HttpStatusCode.NotModified);
					}

					item.upd_date = DateTime.Now;
					item.upd_syain_no = this.UserNum;

					db.SubmitChanges();

					return this.Ok(item);
				}
			} catch (InvalidOperationException ex) {
				return this.Forbidden(ex);
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		/// <summary>
		/// [PUT]
		/// 日次勤怠情報の労働時間を更新します。
		/// </summary>
		/// <param name="id">ユーザーID</param>
		/// <param name="year">年度</param>
		/// <param name="month">月度</param>
		/// <returns>応答メッセージを返します。</returns>
		[HttpPut]
		[Route("WorkTime/{id}/{year}/{month}")]
		[ResponseType(typeof(t_work_d))]
		public IHttpActionResult PutWorkTime(string id, int year, int month) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					db.UpdateWorkTime(id, year, month, this.UserNum);
				}

				return this.Ok();
			} catch (InvalidOperationException ex) {
				return this.Forbidden(ex);
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		#endregion

		#region DELETE

		/// <summary>
		/// [DELETE]
		/// ユーザーIDと勤務日を指定して、
		/// 日次勤怠情報の入退室情報を削除します。
		/// </summary>
		/// <param name="id">ユーザーID</param>
		/// <param name="day">勤務日</param>
		/// <returns>応答メッセージを返します。</returns>
		[HttpDelete]
		[Route("Record/{id}/{day}")]
		[ResponseType(typeof(t_work_d))]
		public IHttpActionResult DeleteRecord(string id, DateTime day) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var item = db.t_work_d.SingleOrDefault(d => d.syain_no == id & d.work_day == day);
					if (item == null) {
						return this.NotFound();
					}

					item.upd_date = DateTime.Now;
					item.upd_syain_no = this.UserNum;

					item.access_in_time = null;
					item.access_out_time = null;
					item.work_time = null;

					db.SubmitChanges();

					return this.Ok(item);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		/// <summary>
		/// [DELETE]
		/// ユーザーIDと勤務日を指定して、
		/// 日次勤怠情報を論理削除します。
		/// </summary>
		/// <param name="id">ユーザーID</param>
		/// <param name="day">勤務日</param>
		/// <returns>応答メッセージを返します。</returns>
		[HttpDelete]
		[Route("{id}/{day}")]
		[ResponseType(typeof(t_work_d))]
		public IHttpActionResult DeleteValue(string id, DateTime day) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var item = db.t_work_d.SingleOrDefault(d => d.syain_no == id & d.work_day == day);
					if (item == null) {
						return this.NotFound();
					}

					item.del_date = DateTime.Now;
					item.del_syain_no = this.UserNum;

					db.SubmitChanges();

					return this.Ok(item);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		/// <summary>
		/// [DELETE]
		/// ユーザーIDと勤務日を指定して、
		/// 日次勤怠情報を物理削除します。
		/// </summary>
		/// <param name="id">ユーザーID</param>
		/// <param name="day">勤務日</param>
		/// <returns>応答メッセージを返します。</returns>
		[HttpDelete]
		[Route("Deregister/{id}/{day}")]
		[ResponseType(typeof(t_work_d))]
		public IHttpActionResult DeregisterValue(string id, DateTime day) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var item = db.t_work_d.SingleOrDefault(d => d.syain_no == id & d.work_day == day);
					if (item == null) {
						return this.NotFound();
					}

					db.t_work_d.DeleteOnSubmit(item);

					db.SubmitChanges();

					return this.StatusCode(HttpStatusCode.NoContent);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		#endregion

		#endregion
	}
}
