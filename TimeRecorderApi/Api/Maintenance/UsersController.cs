﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using AmsDatabaseConnectionLibrary;
using ExtensionsLibrary.Extensions;
using TimeRecorderApi.Api.Primitive;
using static System.Web.VirtualPathUtility;

namespace TimeRecorderApi.Api.Maintenance {
	/// <summary>
	/// ユーザー情報のWeb API を提供します。
	/// </summary>
	[RoutePrefix("Api/Users")]
	public class UsersController : ApiControllerBase {
		#region プロパティ

		/// <summary>
		/// ユーザーID
		/// </summary>
		/// <remarks>
		/// API から変更された時用のユーザーIDです。
		/// </remarks>
		public string UserId => "Api/Users";

		#endregion

		#region メソッド

		#region GET

		/// <summary>
		/// [GET]
		/// </summary>
		/// <returns>ユーザー情報のコレクション</returns>
		[HttpGet]
		[Route("")]
		public IEnumerable<m_user> GetUsers() {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					return db.GetEnrolledUsers().ToList();
				}
			} catch (Exception) {
				return Enumerable.Empty<m_user>();
			}
		}

		/// <summary>
		/// [GET]
		/// </summary>
		/// <param name="id">ユーザーID</param>
		/// <returns>ユーザー情報</returns>
		[HttpGet]
		[Route("{id}")]
		[ResponseType(typeof(m_user))]
		public IHttpActionResult GetUserById(string id) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var user = db.GetEnrolledUser(id);
					if (user == null) {
						return this.NotFound();
					}

					return this.Ok(user);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		/// <summary>
		/// [GET]
		/// カード番号を指定して
		/// 登録されているユーザー情報を取得します。
		/// </summary>
		/// <param name="card">カード番号</param>
		/// <returns>ユーザー情報</returns>
		[HttpGet]
		[Route("ByCard/{card}")]
		[ResponseType(typeof(m_user))]
		public IHttpActionResult GetUserByCard(string card) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var user = db.GetUserByCard(card);
					if (user == null) {
						return this.NotFound();
					}

					return this.Ok(user);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		#endregion

		#region POST

		/// <summary>
		/// [POST]
		/// </summary>
		/// <param name="value">ユーザー情報</param>
		[HttpPost]
		[Route("")]
		[ResponseType(typeof(m_user))]
		public IHttpActionResult PostUser([FromBody()] m_user value) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var id = value.syain_no;
					var user = value.user_id;
					if (id == null || user == null) {
						return this.Forbidden("必須項目が設定されていません。");
					}

					var root = AppendTrailingSlash(this.Request.RequestUri.AbsoluteUri);
					var location = $"{root}{id}";
					if (db.m_user.Any(u => u.syain_no == id)) {
						return this.Redirect(location);
					}

					// 作成日時
					var created = DateTime.Now;

					value.ins_date = created;
					value.ins_syain_no = this.UserId;
					value.upd_date = created;
					value.upd_syain_no = this.UserId;

					db.m_user.InsertOnSubmit(value);

					db.SubmitChanges();

					return this.Created(location, value);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		#endregion

		#region PUT

		/// <summary>
		/// [PUT]
		/// ユーザーIDを指定して
		/// ユーザー情報を更新します。
		/// </summary>
		/// <param name="id">ユーザーID</param>
		/// <param name="value">ユーザー情報</param>
		[HttpPut]
		[Route("{id}")]
		[ResponseType(typeof(m_user))]
		public IHttpActionResult PutUser(string id, [FromBody()] m_user value) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var user = db.m_user.SingleOrDefault(u => u.syain_no == id);
					if (user == null) {
						return this.NotFound();
					}

					var modified = false;

					user.PropertyChanged += (sender, e) => {
						modified = true;
					};

					if (value.del_syain_no.IsWhiteSpace()) {
						user.del_date = null;
						user.del_syain_no = null;
					}

					if (value.org_id != 0) { user.org_id = value.org_id; }
					if (value.belong_cd != null) { user.belong_cd = value.belong_cd; }
					if (value.belong_name != null) { user.belong_name = value.belong_name; }
					if (value.user_id != null) { user.user_id = value.user_id; }
					if (value.user_pass != null) { user.user_pass = value.user_pass; }
					if (value.user_name != null) { user.user_name = value.user_name; }
					if (value.user_name_kana != null) { user.user_name_kana = value.user_name_kana; }
					if (value.main_charge_cd != null) { user.main_charge_cd = value.main_charge_cd; }
					if (value.charge_cd != null) { user.charge_cd = value.charge_cd; }
					if (value.job_cd != null) { user.job_cd = value.job_cd; }
					if (value.notice_mail_adr != null) { user.notice_mail_adr = value.notice_mail_adr; }
					if (value.jisseki_mail_adr != null) { user.jisseki_mail_adr = value.jisseki_mail_adr; }
					if (value.last_login_date.HasValue) { user.last_login_date = value.last_login_date; }
					if (value.this_login_date.HasValue) { user.this_login_date = value.this_login_date; }
					if (value.password_date.HasValue) { user.password_date = value.password_date; }
					if (value.syain_kbn_cd != null) { user.syain_kbn_cd = value.syain_kbn_cd; }
					if (value.kengen_cd != null) { user.kengen_cd = value.kengen_cd; }
					if (value.joukyou_display_order.HasValue) { user.joukyou_display_order = value.joukyou_display_order; }
					if (value.joukyou_display_flag != null) { user.joukyou_display_flag = value.joukyou_display_flag; }
					if (value.overtime_calc_kbn_cd != null) { user.overtime_calc_kbn_cd = value.overtime_calc_kbn_cd; }
					if (value.work_time.HasValue) { user.work_time = value.work_time; }
					if (value.rest_time.HasValue) { user.rest_time = value.rest_time; }
					if (value.card_id != null) { user.card_id = value.card_id; }
					if (value.daily_amount.HasValue) { user.daily_amount = value.daily_amount; }

					if (!modified) {
						return this.StatusCode(HttpStatusCode.NotModified);
					}

					user.upd_date = DateTime.Now;
					user.upd_syain_no = this.UserId;

					db.SubmitChanges();

					return this.Ok(user);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		/// <summary>
		/// [PUT]
		/// ユーザーIDを指定して
		/// 論理削除されたユーザー情報を復帰させます。
		/// </summary>
		/// <param name="id">ユーザーID</param>
		[HttpPut]
		[Route("Recover/{id}")]
		[ResponseType(typeof(m_user))]
		public IHttpActionResult RecoverUser(string id)
			=> this.PutUser(id, new m_user() {
				del_syain_no = string.Empty,
			});

		#endregion

		#region DELETE

		/// <summary>
		/// [DELETE]
		/// ユーザーIDを指定して
		/// ユーザー情報を論理削除します。
		/// </summary>
		/// <param name="id">ユーザーID</param>
		[HttpDelete]
		[Route("{id}")]
		[ResponseType(typeof(m_user))]
		public IHttpActionResult DeleteUser(string id) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var user = db.GetEnrolledUser(id);
					if (user == null) {
						return this.NotFound();
					}

					user.del_date = DateTime.Now;
					user.del_syain_no = this.UserId;

					db.SubmitChanges();

					return this.Ok(user);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		/// <summary>
		/// [DELETE]
		/// ユーザーIDを指定して
		/// ユーザー情報を物理削除します。
		/// </summary>
		/// <param name="id">ユーザーID</param>
		[HttpDelete]
		[Route("Deregister/{id}")]
		[ResponseType(typeof(m_user))]
		public IHttpActionResult DeregisterUser(string id) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var user = db.m_user.SingleOrDefault(u => u.syain_no == id);
					if (user == null) {
						return this.NotFound();
					}

					db.m_user.DeleteOnSubmit(user);

					db.SubmitChanges();

					return this.StatusCode(HttpStatusCode.NoContent);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		#endregion

		#endregion
	}
}
