﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using AmsDatabaseConnectionLibrary;
using ExtensionsLibrary.Extensions;
using static System.Web.VirtualPathUtility;

namespace TimeRecorderApi.Api.Maintenance {
	/// <summary>
	/// 組織情報のWeb API を提供します。
	/// </summary>
	[RoutePrefix("Api/Orgs")]
	public class OrgsController : ApiController {
		#region プロパティ

		/// <summary>
		/// ユーザーID
		/// </summary>
		/// <remarks>
		/// API から変更された時用のユーザーIDです。
		/// </remarks>
		public string UserId => "Api/Orgs";

		#endregion

		#region メソッド

		#region GET

		/// <summary>
		/// [GET]
		/// </summary>
		/// <returns>組織情報のコレクション</returns>
		[HttpGet]
		[Route("")]
		public IEnumerable<m_org> GetOrgs() {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					return db.m_org.GetValidItems().ToList();
				}
			} catch (Exception) {
				return Enumerable.Empty<m_org>();
			}
		}

		/// <summary>
		/// [GET]
		/// </summary>
		/// <param name="id">組織ID</param>
		/// <returns>組織情報</returns>
		[HttpGet]
		[Route("{id}")]
		[ResponseType(typeof(m_org))]
		public IHttpActionResult GetOrgById(int id) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var org = db.m_org.GetValidItems().SingleOrDefault(o => o.org_id == id);
					if (org == null) {
						return this.NotFound();
					}

					return this.Ok(org);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		#endregion

		#region POST

		/// <summary>
		/// [POST]
		/// </summary>
		/// <param name="value">組織情報</param>
		[HttpPost]
		[Route("")]
		[ResponseType(typeof(m_org))]
		public IHttpActionResult PostUser([FromBody()] m_org value) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var root = AppendTrailingSlash(this.Request.RequestUri.AbsoluteUri);
					var id = value.org_id;
					var location = $"{root}{id}";
					if (db.m_org.Any(u => u.org_id == id)) {
						return this.Redirect(location);
					}

					// 作成日時
					var created = DateTime.Now;

					value.ins_date = created;
					value.ins_syain_no = this.UserId;
					value.upd_date = created;
					value.upd_syain_no = this.UserId;

					db.m_org.InsertOnSubmit(value);

					db.SubmitChanges();

					return this.Created(location, value);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		#endregion

		#region PUT

		/// <summary>
		/// [PUT]
		/// 組織IDを指定して
		/// 組織情報を更新します。
		/// </summary>
		/// <param name="id">組織ID</param>
		/// <param name="value">組織情報</param>
		[HttpPut]
		[Route("{id}")]
		[ResponseType(typeof(m_org))]
		public IHttpActionResult PutUser(int id, [FromBody()] m_org value) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var org = db.m_org.SingleOrDefault(u => u.org_id == id);
					if (org == null) {
						return this.NotFound();
					}

					var modified = false;

					org.PropertyChanged += (sender, e) => {
						modified = true;
					};

					if (value.del_syain_no.IsWhiteSpace()) {
						org.del_date = null;
						org.del_syain_no = null;
					}

					if (value.org_id != 0) { org.org_id = value.org_id; }
					if (value.parent_org_id.HasValue || org.parent_org_id.HasValue) { org.parent_org_id = value.parent_org_id; }
					if (value.org_name != null) { org.org_name = value.org_name; }
					if (value.alias != null) { org.alias = value.alias; }
					if (value.display_order.HasValue || org.display_order.HasValue) { org.display_order = value.display_order; }

					if (!modified) {
						return this.StatusCode(HttpStatusCode.NotModified);
					}

					org.upd_date = DateTime.Now;
					org.upd_syain_no = this.UserId;

					db.SubmitChanges();

					return this.Ok(org);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		/// <summary>
		/// [PUT]
		/// 組織IDを指定して
		/// 論理削除された組織情報を復帰させます。
		/// </summary>
		/// <param name="id">組織ID</param>
		[HttpPut]
		[Route("Recover/{id}")]
		[ResponseType(typeof(m_org))]
		public IHttpActionResult RecoverUser(int id)
			=> this.PutUser(id, new m_org() {
				del_syain_no = string.Empty,
			});

		#endregion

		#region DELETE

		/// <summary>
		/// [DELETE]
		/// 組織IDを指定して
		/// 組織情報を論理削除します。
		/// </summary>
		/// <param name="id">組織ID</param>
		[HttpDelete]
		[Route("{id}")]
		[ResponseType(typeof(m_org))]
		public IHttpActionResult DeleteUser(int id) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var org = db.m_org.GetValidItems().SingleOrDefault(o => o.org_id == id);
					if (org == null) {
						return this.NotFound();
					}

					org.del_date = DateTime.Now;
					org.del_syain_no = this.UserId;

					db.SubmitChanges();

					return this.Ok(org);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		/// <summary>
		/// [DELETE]
		/// 組織IDを指定して
		/// 組織情報を物理削除します。
		/// </summary>
		/// <param name="id">組織ID</param>
		[HttpDelete]
		[Route("Deregister/{id}")]
		[ResponseType(typeof(m_org))]
		public IHttpActionResult DeregisterUser(int id) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var user = db.m_org.SingleOrDefault(u => u.org_id == id);
					if (user == null) {
						return this.NotFound();
					}

					db.m_org.DeleteOnSubmit(user);

					db.SubmitChanges();

					return this.StatusCode(HttpStatusCode.NoContent);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		#endregion

		#endregion
	}
}
