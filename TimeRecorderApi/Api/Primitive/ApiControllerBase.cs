﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TimeRecorderApi.Api.Primitive {
	/// <summary>
	/// API コントローラーの基本となる抽象クラスです。
	/// </summary>
	public abstract class ApiControllerBase : ApiController {
		#region コンストラクタ

		/// <summary>
		/// コンストラクタ
		/// </summary>
		protected ApiControllerBase() : base() {
		}

		#endregion

		#region メソッド

		#region 403 Forbidden

		/// <summary>
		/// 指定されたエラー メッセージを使用して、
		/// エラー応答メッセージ (403 Forbidden) を作成します。
		/// </summary>
		/// <param name="message">メッセージ</param>
		/// <returns>作成した応答メッセージ (403 Forbidden) を返します。</returns>
		protected internal virtual IHttpActionResult Forbidden(string message)
			=> this.CreateErrorMessage(HttpStatusCode.Forbidden, message);

		/// <summary>
		/// 指定された例外を使用して、
		/// エラー応答メッセージ (403 Forbidden) を作成します。
		/// </summary>
		/// <param name="exception">例外</param>
		/// <returns>作成した応答メッセージ (403 Forbidden) を返します。</returns>
		protected internal virtual IHttpActionResult Forbidden(Exception exception)
			=> this.CreateErrorMessage(HttpStatusCode.Forbidden, exception);

		#endregion

		#region エラー応答メッセージ作成

		/// <summary>
		/// ステータスコードとメッセージを指定して、
		/// エラー応答メッセージを作成します。
		/// </summary>
		/// <param name="statusCode">ステータスコード</param>
		/// <param name="message">メッセージ</param>
		/// <returns>作成した応答メッセージを返します。</returns>
		protected internal virtual IHttpActionResult CreateErrorMessage(HttpStatusCode statusCode, string message)
			=> this.ResponseMessage(this.Request.CreateErrorResponse(statusCode, message));

		/// <summary>
		/// ステータスコードと例外を指定して、
		/// エラー応答メッセージを作成します。
		/// </summary>
		/// <param name="statusCode">ステータスコード</param>
		/// <param name="exception">例外</param>
		/// <returns>作成した応答メッセージを返します。</returns>
		protected internal virtual IHttpActionResult CreateErrorMessage(HttpStatusCode statusCode, Exception exception)
			=> this.ResponseMessage(this.Request.CreateErrorResponse(statusCode, exception));

		#endregion

		#endregion
	}
}