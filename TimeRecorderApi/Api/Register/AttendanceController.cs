﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using AmsDatabaseConnectionLibrary;
using TimeRecorderApi.Api.Primitive;

namespace TimeRecorderApi.Api.Register {
	/// <summary>
	/// 出退勤時刻登録のWeb API を提供します。
	/// </summary>
	[RoutePrefix("Api/Attendances")]
	public class AttendanceController : ApiControllerBase {
		#region メソッド

		#region GET

		/// <summary>
		/// [GET]
		/// </summary>
		/// <returns>出退勤時刻情報のコレクション</returns>
		[HttpGet]
		[Route("")]
		public IEnumerable<ATTENDANCE> GetValues() {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					return db.ATTENDANCE.ToList();
				}
			} catch (Exception) {
				return Enumerable.Empty<ATTENDANCE>();
			}
		}

		/// <summary>
		/// [GET]
		/// </summary>
		/// <param name="id">ユーザーID</param>
		/// <param name="workDate">日付</param>
		/// <returns>出退勤時刻情報</returns>
		[HttpGet]
		[Route("{id}/{workDate}")]
		[ResponseType(typeof(IEnumerable<ATTENDANCE>))]
		public IHttpActionResult GetValues(string id, DateTime workDate) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var query = (
						from a in db.ATTENDANCE
						where
							a.USER_ID == id
							&& a.WORK_DATE == workDate
						select a
					);

					if (!query.Any()) {
						return this.NotFound();
					}

					var items = query.ToList();
					return this.Ok(items);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		#endregion

		#region POST

		/// <summary>
		/// [POST]
		/// </summary>
		/// <param name="value">出退勤時刻情報</param>
		[HttpPost]
		[Route("")]
		public IHttpActionResult PostValue([FromBody()] ATTENDANCE value) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					db.ATTENDANCE.InsertOnSubmit(value);

					db.SubmitChanges();
				}

				return this.Ok();
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		/// <summary>
		/// [POST]
		/// </summary>
		/// <param name="card">カード番号</param>
		[HttpPost]
		[Route("{card}")]
		[ResponseType(typeof(ATTENDANCE))]
		public IHttpActionResult PostByCardNum(string card) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var user = db.GetUserByCard(card);
					if (user == null) {
						return this.BadRequest($"指定されたカード番号のユーザーが見つかりません。card={card}");
					}

					var id = user.syain_no;
					if (db.ATTENDANCE.Any(a => a.USER_ID == id && !a.EXIT_TIME.HasValue)) {
						return this.Forbidden("退勤時刻が登録されていないデータがあります。");
					}

					var entry = DateTime.Now;

					var value = new ATTENDANCE() {
						USER_ID = user.syain_no,
						WORK_DATE = entry.Date,
						ENTRY_TIME = entry,
						CARD_NUM = card,
						USER_NAME = user.user_name,
					};

					db.ATTENDANCE.InsertOnSubmit(value);

					db.SubmitChanges();

					return this.Ok(value);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		#endregion

		#region PUT

		/// <summary>
		/// [PUT]
		/// </summary>
		/// <param name="id">ユーザーID</param>
		/// <param name="workDate">日付</param>
		/// <param name="entryTime">出勤時刻</param>
		/// <param name="value">出退勤時刻情報</param>
		[HttpPut]
		[Route("{id}/{workDate}/{entryTime}")]
		[ResponseType(typeof(ATTENDANCE))]
		public IHttpActionResult PutValue(string id, DateTime workDate, DateTime entryTime, [FromBody()] ATTENDANCE value) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var item = (
						from a in db.ATTENDANCE
						where
							a.USER_ID == id
							&& a.WORK_DATE == workDate
							&& a.ENTRY_TIME == entryTime
						select a
					).SingleOrDefault();
					if (item == null) {
						return this.NotFound();
					}

					if (value.CARD_NUM != null) { item.CARD_NUM = value.CARD_NUM; }
					if (value.USER_NAME != null) { item.USER_NAME = value.USER_NAME; }
					if (value.ENTRY_TIME != DateTime.MinValue) { item.ENTRY_TIME = value.ENTRY_TIME; }
					if (value.EXIT_TIME.HasValue) { item.EXIT_TIME = value.EXIT_TIME; }

					db.SubmitChanges();

					return this.Ok(item);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		/// <summary>
		/// [PUT]
		/// </summary>
		/// <param name="card">カード番号</param>
		[HttpPut]
		[Route("{card}")]
		[ResponseType(typeof(ATTENDANCE))]
		public IHttpActionResult PutByCardNum(string card) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var user = db.GetUserByCard(card);
					if (user == null) {
						return this.BadRequest($"指定されたカード番号のユーザーが見つかりません。card={card}");
					}

					var id = user.syain_no;
					var item = db.ATTENDANCE.SingleOrDefault(a => a.USER_ID == id && !a.EXIT_TIME.HasValue);
					if (item == null) {
						return this.NotFound();
					}

					var exit = DateTime.Now;

					item.EXIT_TIME = exit;

					db.SubmitChanges();

					return this.Ok(item);
				}
			} catch (InvalidOperationException ex) {
				return this.Forbidden(ex);
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		#endregion

		#region DELETE

		/// <summary>
		/// [DELETE]
		/// ユーザーIDと日付を指定して
		/// ユーザー情報を物理削除します。
		/// </summary>
		/// <param name="id">ユーザーID</param>
		/// <param name="workDate">日付</param>
		[HttpDelete]
		[Route("Deregister/{id}/{workDate}")]
		[ResponseType(typeof(m_user))]
		public IHttpActionResult DeregisterValue(string id, DateTime workDate) {
			try {
				using (var db = new AmsDataClassesDataContext()) {
					var query = (
						from a in db.ATTENDANCE
						where
							a.USER_ID == id
							&& a.WORK_DATE == workDate
						select a
					);

					if (!query.Any()) {
						return this.NotFound();
					}

					db.ATTENDANCE.DeleteAllOnSubmit(query);

					db.SubmitChanges();

					return this.StatusCode(HttpStatusCode.NoContent);
				}
			} catch (Exception ex) {
				return this.InternalServerError(ex);
			}
		}

		#endregion

		#endregion
	}
}