﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using AmsDatabaseConnectionLibrary;
using AmsDatabaseConnectionLibrary.Model.Common;
using ExtensionsLibrary.Extensions;
using JsonLibrary.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TimeRecorderApi.Api.Maintenance;
using TimeRecorderApi.Api.Register;

namespace TimeRecorderApi.Tests {
	/// <summary>
	/// Web API の単体テストを行うクラスです。
	/// </summary>
	/// <remarks>
	/// <para>[前提条件]</para>
	/// <para>ソースを改修したら、テストで参照するサイトを再発行して下さい。</para>
	/// </remarks>
	[TestClass]
	public class UnitTestApi {
		#region フィールド

		private string _site = @"http://vmypc2016/AMS_API";
		private string _con = "Data Source = VMYPC2016; Initial Catalog = AMS_YDM; Persist Security Info=True;User ID = sa; Password=admin-VMYPC2016-local";

		#endregion

		#region メソッド

		#region ユーザー情報

		[TestMethod]
		[Owner(nameof(UsersController))]
		[TestCategory("取得")]
		public async Task ユーザー情報一覧取得() {
			var path = $"{this._site}/Api/Users";

			using (var httpClient = new HttpClient()) {
				var response = await httpClient.GetAsync(path);

				// 1: HTTPステータスコードがOK(200)で返ってくること
				Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

				var contents = await response.Content.ReadAsStringAsync();
				var users = contents.Deserialize<IEnumerable<m_user>>();

				// 2: 取得したユーザー情報が存在していること
				Assert.IsTrue(users.Any());

				// 3: 取得したユーザー件数が20件であること
				Assert.AreEqual(20, users.Count());
			}
		}

		[TestMethod]
		[Owner(nameof(UsersController))]
		[TestCategory("取得")]
		public async Task ユーザー情報取得_ID() {
			{// 正常系
				var id = "0000000001";
				var path = $"{this._site}/Api/Users/{id}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.GetAsync(path);

					// 4: HTTPステータスコードがOK(200)で返ってくること
					Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

					var contents = await response.Content.ReadAsStringAsync();
					var user = contents.Deserialize<m_user>();

					// 5: 取得したユーザー名が「松崎真由美」であること
					Assert.AreEqual("松崎真由美", user.user_name);
				}
			}
			{// 異常系
				var id = "BBBBBBBBBB";
				var path = $"{this._site}/Api/Users/{id}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.GetAsync(path);

					// 6: HTTPステータスコードがNotFound(404)で返ってくること
					Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
				}
			}
		}

		[TestMethod]
		[Owner(nameof(UsersController))]
		[TestCategory("取得")]
		public async Task ユーザー情報取得_カード() {
			{// 正常系
				var card = "0000000000000001";
				var path = $"{this._site}/Api/Users/ByCard/{card}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.GetAsync(path);

					// 7: HTTPステータスコードがOK(200)で返ってくること
					Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

					var contents = await response.Content.ReadAsStringAsync();
					var user = contents.Deserialize<m_user>();

					// 8: 取得したユーザー名が「松崎真由美」であること
					Assert.AreEqual("松崎真由美", user.user_name);

				}
			}
			{// 異常系
				var card = "BBBBBBBBBBBBBBBB";
				var path = $"{this._site}/Api/Users/ByCard/{card}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.GetAsync(path);

					// 9: HTTPステータスコードがNotFound(404)で返ってくること
					Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
				}
			}
		}

		[TestMethod]
		[Owner(nameof(UsersController))]
		[TestCategory("更新")]
		public async Task ユーザー情報更新() {
			{// 正常系
				var id = "0000000002";
				var path = $"{this._site}/Api/Users/{id}";

				// 期待値
				var expected = "1111111111111111";

				var val = new m_user() {
					user_id = expected
				};
				var dic = val.ToPropertyDictionary().ToDictionary(kv => kv.Key, kv => kv.Value?.ToString());
				var content = new FormUrlEncodedContent(dic);

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.PutAsync(path, content);

					// 10: HTTPステータスコードがOK(200)で返ってくること
					Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
					using (var db = new AmsDataClassesDataContext(this._con)) {
						var user = db.GetEnrolledUser(id);

						// 11: 更新した値を持つユーザーが存在すること
						Assert.AreEqual(expected, user.user_id);
					}
				}
			}
			{// 異常系: ユーザー情報に存在しないユーザーIDでカードIDを更新
				var id = "BBBBBBBBBB";
				var path = $"{this._site}/Api/Users/{id}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.PutAsync(path, null);

					// 12: HTTPステータスコードがNotFound(404)で返ってくること
					Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
				}
			}
			{// 異常系: 登録済みの値でユーザー情報を更新
				var id = "0000000002";

				var path = $"{this._site}/Api/Users/{id}";

				var val = new m_user() {
					user_id = "1111111111111111"
				};
				var dic = val.ToPropertyDictionary().ToDictionary(kv => kv.Key, kv => kv.Value?.ToString());
				var content = new FormUrlEncodedContent(dic);

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.PutAsync(path, content);

					// 13: HTTPステータスコードがNotModified(304)で返ってくること
					Assert.AreEqual(HttpStatusCode.NotModified, response.StatusCode);
				}
			}
		}

		[TestMethod]
		[Owner(nameof(UsersController))]
		[TestCategory("削除")]
		public async Task ユーザー情報を論理削除() {
			{// 正常系
				var id = "0000000003";
				var path = $"{this._site}/Api/Users/{id}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.DeleteAsync(path);

					// 14: HTTPステータスコードがOK(200)で返ってくること
					Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
				}
				using (var db = new AmsDataClassesDataContext(this._con)) {
					var user = db.GetEnrolledUser(id);

					// 15: 論理削除したユーザーIDのユーザーが存在しないこと
					Assert.IsNull(user);
				}
			}
			{// 異常系:ユーザー情報に存在しないユーザーIDで論理削除
				var id = "BBBBBBBBBB";
				var path = $"{this._site}/Api/Users/{id}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.DeleteAsync(path);

					// 16: HTTPステータスコードがNotFound(404)で返ってくること
					Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
				}
			}
		}

		[TestMethod]
		[Owner(nameof(UsersController))]
		[TestCategory("更新")]
		public async Task 論理削除されたユーザー情報を復帰() {
			{// 正常系
				var id = "0000000003";

				var path = $"{this._site}/Api/Users/Recover/{id}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.PutAsync(path, null);

					using (var db = new AmsDataClassesDataContext(this._con)) {
						var user = db.GetEnrolledUser(id);

						// 17: 論理削除したユーザーIDのユーザーが存在すること
						Assert.IsNotNull(user);
					}
				}
			}
		}

		[TestMethod]
		[Owner(nameof(UsersController))]
		[TestCategory("削除")]
		public async Task ユーザー情報を物理削除() {
			{// 正常系
				var id = "0000000004";
				var path = $"{this._site}/Api/Users/Deregister/{id}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.DeleteAsync(path);

					// 18: HTTPステータスコードがNoContent(204)で返ってくること
					Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
				}
				using (var db = new AmsDataClassesDataContext(this._con)) {
					var user = db.GetEnrolledUser(id);

					// 19: 物理削除したユーザーIDのユーザーが存在しないこと
					Assert.IsNull(user);
				}
			}
			{// 異常系:ユーザー情報に存在しないユーザーIDで物理削除
				var id = "BBBBBBBBBB";
				var path = $"{this._site}/Api/Users/{id}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.DeleteAsync(path);

					// 20: HTTPステータスコードがNotFound(404)で返ってくること
					Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
				}
			}
		}

		#endregion

		#region 出退勤情報

		[TestMethod]
		[Owner(nameof(AttendanceController))]
		[TestCategory("取得")]
		public async Task 出退勤情報一覧取得() {
			var path = $"{this._site}/Api/Attendances";

			using (var httpClient = new HttpClient()) {
				var response = await httpClient.GetAsync(path);

				// 21: HTTPステータスコードがOK(200)で返ってくること
				Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

				var contents = await response.Content.ReadAsStringAsync();
				var attendance = contents.Deserialize<IEnumerable<ATTENDANCE>>();

				// 22: 取得した出退勤時刻の件数が7件であること
				Assert.AreEqual(7, attendance.Count());
			}
		}

		[TestMethod]
		[Owner(nameof(AttendanceController))]
		[TestCategory("取得")]
		public async Task 出退勤情報取得() {
			{// 正常系
				var id = "0000000001";
				var workDate = "2018-06-20";
				var path = $"{this._site}/Api/Attendances/{id}/{workDate}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.GetAsync(path);

					// 23: HTTPステータスコードがOK(200)で返ってくること
					Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

					var contents = await response.Content.ReadAsStringAsync();
					var attendance = contents.Deserialize<IEnumerable<ATTENDANCE>>();

					foreach (var a in attendance) {
						Console.WriteLine($"{a}");
					}

					// 24: 取得した出退勤時刻の件数が4件であること
					Assert.AreEqual(4, attendance.Count());
				}
			}
			{// 異常系
				var id = "BBBBBBBBBB";
				var workDate = "2018-06-20";
				var path = $"{this._site}/Api/Attendances/{id}/{workDate}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.GetAsync(path);

					// 25: HTTPステータスコードがNotFound(404)で返ってくること
					Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
				}
			}
		}

		[TestMethod]
		[Owner(nameof(AttendanceController))]
		[TestCategory("登録")]
		public async Task 出勤情報登録() {
			{// 正常系
				var card = "0000000000000001";
				var path = $"{this._site}/Api/Attendances/{card}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.PostAsync(path, null);

					// 26: HTTPステータスコードがOK(200)で返ってくること
					Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

					var contents = await response.Content.ReadAsStringAsync();
					var attendance = contents.Deserialize<ATTENDANCE>();

					var today = DateTime.Today;

					// 27: 出勤時刻の値に現在の日付が登録されていること
					Assert.AreEqual(today, attendance.ENTRY_TIME.Date);
				}
			}
			{// 異常系:出勤時刻に紐づく退勤時刻が登録されていない
				var card = "0000000000000001";
				var path = $"{this._site}/Api/Attendances/{card}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.PostAsync(path, null);

					// 28: HTTPステータスコードがForbidden(403)で返ってくること
					Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
				}
			}
			{// 異常系：ユーザー情報に存在しないカード番号で出勤時刻を登録
				var id = "BBBBBBBBBBBBBBBB";
				var path = $"{this._site}/Api/Attendances/{id}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.PostAsync(path, null);

					// 29: HTTPステータスコードがBadRequest(400)で返ってくること
					Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
				}
			}
		}

		[TestMethod]
		[Owner(nameof(AttendanceController))]
		[TestCategory("更新")]
		public async Task 退勤情報更新() {
			{// 正常系
				var card = "0000000000000001";
				var path = $"{this._site}/Api/Attendances/{card}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.PutAsync(path, null);

					// 30: HTTPステータスコードがOK(200)で返ってくること
					Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

					var contents = await response.Content.ReadAsStringAsync();
					var attendance = contents.Deserialize<ATTENDANCE>();

					// 31: 退勤時刻に値が入っていること
					Assert.IsTrue(attendance.EXIT_TIME.HasValue);
				}
			}
			{// 異常系:出勤時刻に紐づく退勤時刻が登録されている
				var card = "0000000000000001";
				var path = $"{this._site}/Api/Attendances/{card}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.PutAsync(path, null);

					// 32: HTTPステータスコードがNotFound(404)で返ってくること
					Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
				}
			}
			{// 異常系：ユーザー情報に存在しないカード番号で退勤時刻を登録
				var id = "BBBBBBBBBBBBBBBB";
				var path = $"{this._site}/Api/Attendances/{id}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.PutAsync(path, null);

					// 33: HTTPステータスコードがBadRequest(400)で返ってくること
					Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
				}
			}
		}

		#endregion

		#region 日次勤怠情報

		[TestMethod]
		[Owner(nameof(WorkDaysController))]
		[TestCategory("取得")]
		public async Task 日次勤怠情報一覧取得() {
			var path = $"{this._site}/Api/WorkDays";

			using (var httpClient = new HttpClient()) {
				var response = await httpClient.GetAsync(path);

				// 34: HTTPステータスコードがOK(200)で返ってくること
				Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

				var contents = await response.Content.ReadAsStringAsync();
				var work = contents.Deserialize<IEnumerable<t_work_d>>();

				foreach (var a in work) {
					Console.WriteLine($"{a}");
				}

				// 35: 取得した日次勤怠情報の件数が20件であること
				Assert.AreEqual(20, work.Count());
			}
		}

		[TestMethod]
		[Owner(nameof(WorkDaysController))]
		[TestCategory("取得")]
		public async Task 日次勤怠情報取得() {
			{// 正常系
				var id = "0000000005";
				var day = "2018-06-20";
				var path = $"{this._site}/Api/WorkDays/{id}/{day}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.GetAsync(path);

					// 36: HTTPステータスコードがOK(200)で返ってくること
					Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

					var contents = await response.Content.ReadAsStringAsync();
					var work = contents.Deserialize<t_work_d>();

					// 37: 取得した日次勤怠情報の社員番号が同じであること
					Assert.AreEqual(id, work.syain_no);
				}
			}
			{// 異常系
				var id = "BBBBBBBBBB";
				var day = "2018-06-20";
				var path = $"{this._site}/Api/WorkDays/{id}/{day}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.GetAsync(path);

					// 38: HTTPステータスコードがNotFound(404)で返ってくること
					Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
				}
			}
		}

		[TestMethod]
		[Owner(nameof(WorkDaysController))]
		[TestCategory("登録")]
		public async Task 日次勤怠情報登録() {
			{// 正常系
				var path = $"{this._site}/Api/WorkDays/";

				// 期待値
				var expected = "0000000030";

				var val = new t_work_d() {
					syain_no = expected,
					nendo = DateTime.Now.Year.ToString(),
					getsudo = DateTime.Now.Day.ToString(),
					work_day = DateTime.Today,
					kintai_kbn_cd = "01"
				};
				var dic = val.ToPropertyDictionary().ToDictionary(kv => kv.Key, kv => kv.Value?.ToString());
				var content = new FormUrlEncodedContent(dic);

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.PostAsync(path, content);

					// 39: HTTPステータスコードがCreated(201)で返ってくること
					Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);

					var contents = await response.Content.ReadAsStringAsync();
					var work = contents.Deserialize<t_work_d>();

					// 40: 取得した日次勤怠情報の社員番号が同じであること
					Assert.AreEqual(expected, work.syain_no);
				}
			}
			{// 異常系:日次勤怠情報に必須項目が入力されていない
				using (var httpClient = new HttpClient()) {
					var path = $"{this._site}/Api/WorkDays/";

					var val = new t_work_d() {
						nendo = DateTime.Now.Year.ToString(),
						getsudo = DateTime.Now.Day.ToString(),
						work_day = DateTime.Today,
						kintai_kbn_cd = "01"
					};
					var dic = val.ToPropertyDictionary().ToDictionary(kv => kv.Key, kv => kv.Value?.ToString());
					var content = new FormUrlEncodedContent(dic);
					var response = await httpClient.PostAsync(path, content);

					Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
				}
			}
		}

		[TestMethod]
		[Owner(nameof(WorkDaysController))]
		[TestCategory("更新")]
		public async Task 日次勤怠情報更新() {
			{// 正常系
				var id = "0000000030";
				var day = DateTime.Today;
				var path = $"{this._site}/Api/WorkDays/{id}/{day.ToString("yyyy-MM-dd")}";

				// 期待値
				var expected = "08:00";

				var val = new t_work_d() {
					kouban_in_time = expected
				};
				var dic = val.ToPropertyDictionary().ToDictionary(kv => kv.Key, kv => kv.Value?.ToString());
				var content = new FormUrlEncodedContent(dic);

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.PutAsync(path, content);

					// 42: HTTPステータスコードがOK(200)で返ってくること
					Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

					var contents = await response.Content.ReadAsStringAsync();
					var work = contents.Deserialize<t_work_d>();

					// 43:  日次勤怠情報の交番出勤時刻が「08:00」で返ってくること
					Assert.AreEqual(expected, work.kouban_in_time);
				}
			}
			{// 異常系：ユーザー情報に存在しないカード番号で 日次勤怠情報を更新
				var id = "BBBBBBBBBBBBBBBB";
				var day = DateTime.Today;
				var path = $"{this._site}/Api/WorkDays/{id}/{day.ToString("yyyy-MM-dd")}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.PutAsync(path, null);

					// 44: HTTPステータスコードがNotFound(404)で返ってくること
					Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
				}
			}
		}

		[TestMethod]
		[Owner(nameof(WorkDaysController))]
		[TestCategory("更新")]
		public async Task 登録済みの情報で日次勤怠情報更新() {
			{// 異常系
				var id = "0000000030";
				var day = DateTime.Today;
				var path = $"{this._site}/Api/WorkDays/{id}/{day.ToString("yyyy-MM-dd")}";

				// 期待値
				var expected = "08:00";

				var val = new t_work_d() {
					kouban_in_time = expected
				};
				var dic = val.ToPropertyDictionary().ToDictionary(kv => kv.Key, kv => kv.Value?.ToString());
				var content = new FormUrlEncodedContent(dic);

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.PutAsync(path, content);

					// 45: HTTPステータスコードがNotModified(304)で返ってくること
					Assert.AreEqual(HttpStatusCode.NotModified, response.StatusCode);
				}
			}
		}

		[TestMethod]
		[Owner(nameof(WorkDaysController))]
		[TestCategory("更新")]
		public async Task 日次勤怠情報入退室情報記録() {
			{// 異常系
				var id = "019165";
				var day = new DateTime(2018, 07, 24);
				var path = $"{this._site}/Api/WorkDays/Record/{id}/{day.ToString("yyyy-MM-dd")}";

				// 期待値
				var expected = "08:00";

				var val = new t_work_d() {
					access_in_time = expected,
					access_out_time = "17:30",
				};

				var dic = val.ToPropertyDictionary().ToDictionary(kv => kv.Key, kv => kv.Value?.ToString());
				var content = new FormUrlEncodedContent(dic);

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.PutAsync(path, content);

					// HTTPステータスコードがOK(200)で返ってくること
					Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

					var contents = await response.Content.ReadAsStringAsync();
					var work = contents.Deserialize<AttendanceInfo>();

					// 日次勤怠情報の交番出勤時刻が「08:00」で返ってくること
					Assert.AreEqual(expected, work.access_in_time);
				}
			}
		}

		[TestMethod]
		[Owner(nameof(WorkDaysController))]
		[TestCategory("削除")]
		public async Task 日次勤怠情報論理削除() {
			{// 正常系
				var id = "0000000030";
				var day = DateTime.Today;
				var path = $"{this._site}/Api/WorkDays/{id}/{day.ToString("yyyy-MM-dd")}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.DeleteAsync(path);

					// 46: HTTPステータスコードがOK(200)で返ってくること
					Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

					var contents = await response.Content.ReadAsStringAsync();
					var work = contents.Deserialize<t_work_d>();

					// 47: 削除日時に値が入っていること
					Assert.IsTrue(work.del_date.HasValue);
				}
			}
			{// 異常系：日次勤怠情報に存在しない社員番号で日次勤怠情報を論理削除
				var id = "BBBBBBBBBBBBBBBB";
				var day = DateTime.Today;
				var path = $"{this._site}/Api/WorkDays/{id}/{day.ToString("yyyy-MM-dd")}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.PutAsync(path, null);

					// 48: HTTPステータスコードがNotFound(404)で返ってくること
					Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
				}
			}
		}

		[TestMethod]
		[Owner(nameof(WorkDaysController))]
		[TestCategory("削除")]
		public async Task 日次勤怠情報物理削除() {
			{// 正常系：日次勤怠情報に存在する社員番号で日次勤怠情報を物理削除
				var id = "0000000030";
				var day = DateTime.Today;
				var path = $"{this._site}/Api/WorkDays/Deregister/{id}/{day.ToString("yyyy-MM-dd")}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.DeleteAsync(path);

					// 49: HTTPステータスコードがNoContent(204)で返ってくること
					Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);

					var contents = await response.Content.ReadAsStringAsync();
					var work = contents.Deserialize<t_work_d>();
				}
			}
			{// 正常系：物理削除した社員番号で日時勤怠情報を取得
				var id = "0000000030";
				var day = DateTime.Today;
				var path = $"{this._site}/Api/WorkDays/{id}/{day.ToString("yyyy-MM-dd")}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.GetAsync(path);

					// 50: HTTPステータスコードがNotFound(404)で返ってくること
					Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
				}
			}
			{// 異常系：日次勤怠情報に存在しない社員番号で日次勤怠情報を物理削除
				var id = "BBBBBBBBBBBBBBBB";
				var day = DateTime.Today;
				var path = $"{this._site}/Api/WorkDays/{id}/{day.ToString("yyyy-MM-dd")}";

				using (var httpClient = new HttpClient()) {
					var response = await httpClient.PutAsync(path, null);

					// 51: HTTPステータスコードがNotFound(404)で返ってくること
					Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
				}
			}
		}

		#endregion

		#endregion
	}
}
